import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './style.css'

class TabbedDisplay extends Component {
    render() {
        return (
            <div>
                <div className="container"><h2>Example tab 2 (using standard nav-tabs)</h2></div>

                <div id="exTab2" className="container">
                    <ul className="nav nav-tabs">
                        <li className="active">
                            <a  href="#1" data-toggle="tab">Overview</a>
                        </li>
                        <li><a href="#2" data-toggle="tab">Without clearfix</a>
                        </li>
                        <li><a href="#3" data-toggle="tab">Solution</a>
                        </li>
                    </ul>

                    <div className="tab-content ">
                        <div className="tab-pane active" id="1">
                            <h3>Standard tab panel created on bootstrap using nav-tabs</h3>
                            { this.props.tab1 }
                        </div>
                        <div className="tab-pane" id="2">
                            <h3>Notice the gap between the content and tab after applying a background color</h3>
                        </div>
                        <div className="tab-pane" id="3">
                            <h3>add clearfix to tab-content (see the css)</h3>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

TabbedDisplay.propTypes = {    // instead of getInitialProps()
    tab1: PropTypes.object
};
TabbedDisplay.defaultProps = {   // instead of getDefaultProps()

};

export default TabbedDisplay

