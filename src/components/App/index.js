import React, { Component } from 'react';
import logo from './logo.svg';
import './style.css';
import TabbedDisplay from '../TabbedDisplay'

import { connect } from 'react-redux'
import { actionCreators } from './../../cat-redux'

//import PagedCats from '../PagedCats' // add later

const apiKey = 'MjMwNDg0'

const mapStateToProps = (state) => ({
    todos: state.todos,
    apiKey: apiKey
})

class App extends Component {
    render() {
        return (
            <div className="App">
                <TabbedDisplay tab1={<PassedComponent />}/>
                {/* <TabbedDisplay tab1={<PagedCats types={'gif'} />}/>   Later */}
            </div>
        );
    }
}

class PassedComponent extends Component {
    render() {
        return (
            <div>Here is some component text</div>
        )
    }
}
export default connect(mapStateToProps)(App)

