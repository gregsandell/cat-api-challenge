import React from 'react'
import fetch from 'node-fetch'
import xml2js from 'xml2js'

const myFetch = () => {
    fetch('http://localhost:8080/api/images/get?api_key=MjMwNDg0&format=xml&results_per_page=20')
        .then(function (response) {
            return response.text()
        }).then(function (xmlString) {
            xml2js.parseString(xmlString, function (err, result) {
                console.dir('json = ' + JSON.stringify(result))
            })
        }).
        catch(function (e) {
            console.log(`caught error, message = ${e.message}`)
})
}
export default class About extends React.Component {

    render() {
        const data = myFetch()

        return (
            <div>
                {data}
            </div>
        )
    }

}

