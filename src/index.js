import React from 'react';
import { render } from 'react-dom'
import './index.css';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import { createStore, combineReducers } from 'redux'
import { Provider } from 'react-redux'
import { Switch, Router, Route } from 'react-router'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'
import { BrowserRouter } from 'react-router-dom'
import About from './components/About';
import NotFound from './components/NotFound';

import { reducers } from './cat-redux'

const store = createStore(
    combineReducers({
        ...reducers,
        routing: routerReducer
    })
)

const Routes = (history, props) => (
    <BrowserRouter {...props}>
        <Switch>
            <Route exact path='/' component={App}/>
            <Route path='/about' component={About}/>
            <Route component={NotFound}/>
        </Switch>
    </BrowserRouter>
);


const AppWithStore = (
    <Provider store={store}>
        <Routes />
    </Provider>
)

render(AppWithStore, document.getElementById('root'))
registerServiceWorker();

