'use strict';

module.exports = {
    context: __dirname,
    entry: '../proxy-server/app.js',
    devServer: {
        proxy: {
            '/api/*': {
                target: 'http://thecatapi.com',
                changeOrigin: true
            }
        },
        headers: { "Access-Control-Allow-Origin": "*" }
    }

}
