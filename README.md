# Programming Challenge

## Read first
Code does not build due to problems never figured out.
Branch paged-table was an effort but failed.
Abandoning project.  GJS 2021-01-14

## Build & Run Instructions

1. Run *npm i*
2. Go to a terminal window and run *npm run start-proxy*
3. Go to a 2nd terminal window and run *npm start*
4. Load *http://localhost:3000* in browser

## Original Instructions

Hello Greg,

Thank you again for interviewing with us. The purpose of this assigment is for
you to demonstrate your coding skills. We hope to see code quality, design
decisions, as well as the use of testing under development.

## Assignment

You will be working with the following API:

- http://thecatapi.com/

Please read and familiarize yourself with the documentation and features. Your
responsibility will be to digest and interact with the data.

### Requirements

- You must use React and Redux
- Generate an API authentication token and manage it within the application

- Build a UI that will display the data digested from the API

- Provide a section with only jpeg or png images that support pagination
- Provide a section with only gif images that support pagination
- Provide a section that display an image from each category
- Allow the user to favorite an image (i.e. jpeg/png and gifs)
- Provide an area or page that lists the user's favorites

- Provide a solution or reaction when the API is offline
- Provide a README about this application and how-to-run-it

### Bonus

- Utilize tests when needed (e.g. unit, cases, mocks)
- Supports caching for faster response time
- Delivered as a single-page application
- Provide a build process for running locally and deploying

### Notes

- Do not worry about design. A simple css boilerplate will do.
